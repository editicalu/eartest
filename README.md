# Editicalu ArchLinux Repository

## How it works

The EAR works fully automated. Every day at 2.59 UTC, GitLab CI builds the newest version of every package. If it was built before, no new version should be built.

These packages are signed by the EAR signing key, available in this repo. These are then served through GitLab Pages.

## Beware!

The EAR comes with no warranty! Automated building could give all sorts of security problems. Some include:

- The upstream PKGBUILD could be infected and then packaged without us noticing.
- The private key could be stolen by GitLab, Google, or any affiliate that has access to these servers.
- The private key could be leaked by something else, like the CI log.

I do my best to avoid the problems above, but I cannot avoid them.

## Submitting packages

If you want a package to be included, please open an issue.

However, proprietary packages (like spotify, visual-studio-code-bin, slack-desktop) are not allowed in the EAR, as their EULA's/licenses do not allow it and I don't want legal problems.

