#!/bin/sh
if [ -z "$MAKEPKG" ]
then
    MAKEPKG="makepkg -si --noconfirm --sign --clean || exit $?";
else
    echo "makepkg was set to '$MAKEPKG'"
fi

# If the directory does not exist, we'll have to clone the repo.
if [ ! -d $1 ]
then
    echo "Cloning https://aur.archlinux.org/$1.git into $1"
    git clone "https://aur.archlinux.org/$1.git" $1
    echo "cd"
    cd $1
    set -e
    eval $MAKEPKG
else
    cd $1 || exit $?
    git remote update || exit $?

    git remote show origin | grep "local out of date"
    if [ $? == 0 ]; then
        echo "Pulling new version"
        git pull || exit $?
	    find . -name '*.pkg.*' -exec rm {} +
        echo "Starting makepkg"
        set -e
        eval $MAKEPKG
        echo "makepkg is done"
    fi
fi

cd ..
