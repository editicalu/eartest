#!/bin/sh

# Import our own private key
echo "$GPG_SEC" > priv.asc
gpg --import priv.asc

expect -c "spawn gpg --edit-key 85B8F5585ED0E8F6E56D4478A9C4E7734638ACF8 trust quit; send \"5\ry\r\"; expect eof"

sudo pacman-key --init
sudo pacman-key --add public/signingkey.asc
sudo pacman-key --lsign-key eartest@wardsegers.be
